package com.snake.googleauthenticatorexample

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.auth.api.identity.Identity
import com.snake.googleauthenticatorexample.databinding.ActivitySignInBinding
import com.snake.googleauthenticatorexample.signin.GoogleAuthUiClient
import kotlinx.coroutines.launch

class SignInActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignInBinding
    private val googleAuthUiClient by lazy {
        GoogleAuthUiClient(
            context = applicationContext,
            oneTapClient = Identity.getSignInClient(applicationContext)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        if(googleAuthUiClient.getSignedInUser() != null) {
            replaceToHome()
        }

        binding.btnSignIn.setOnClickListener {
            lifecycleScope.launch {
                val signInIntentSender = googleAuthUiClient.signIn()
                registerLogIn.launch(
                    IntentSenderRequest.Builder(
                        signInIntentSender ?: return@launch
                    ).build()
                )
            }
        }

    }

    private fun replaceToHome() {
        val userData = googleAuthUiClient.getSignedInUser()
        val intent = Intent(this@SignInActivity, MainActivity::class.java)
        intent.putExtra(Common.USER_DATA_EXTRA, userData)
        startActivity(intent)
    }

    private val registerLogIn = registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
        if(it.resultCode == RESULT_OK) {
            lifecycleScope.launch {
                val signInResult = googleAuthUiClient.signInWithIntent(
                    intent = it.data ?: return@launch
                )
                if (signInResult.data != null) {
                    Toast.makeText(this@SignInActivity, "Signed In Successfully!", Toast.LENGTH_SHORT).show()
                    replaceToHome()
                } else {
                    Toast.makeText(this@SignInActivity, "Sign In Error: ${signInResult.errorMessage}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


}