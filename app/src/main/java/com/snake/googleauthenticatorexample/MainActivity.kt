package com.snake.googleauthenticatorexample

import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.identity.Identity
import com.snake.googleauthenticatorexample.databinding.ActivityMainBinding
import com.snake.googleauthenticatorexample.signin.GoogleAuthUiClient
import com.snake.googleauthenticatorexample.signin.UserData
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

//    private val APPLICATION_NAME = "com.snake.googleauthenticatorexample"
//    private val JSON_FACTORY: JsonFactory = GsonFactory.getDefaultInstance()
//    private val TOKENS_DIRECTORY_PATH = "tokens"
//    private val SCOPES = listOf(GmailScopes.GMAIL_LABELS)
//    private val CREDENTIALS_FILE_PATH = "/credentials.json"

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val userData = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra(Common.USER_DATA_EXTRA, UserData::class.java)
        } else {
            intent.getParcelableExtra(Common.USER_DATA_EXTRA)
        }

        userData?.let { data ->
            data.profilePictureUrl?.let { url ->
                Glide.with(this@MainActivity).load(url).into(binding.ivAvatar)
            }
            data.username?.let { name ->
                binding.tvName.text = name
            }
            data.userId?.let { id ->
                binding.tvID.text = id
            }
            binding.btnSignOut.setOnClickListener {
                lifecycleScope.launch {
                    GoogleAuthUiClient(
                        context = applicationContext,
                        oneTapClient = Identity.getSignInClient(applicationContext)
                    ).signOut()
                    Toast.makeText(this@MainActivity, "Signed Out Successfully!", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }

    }

}